<!-- in /templates/Users/login.php -->



<div class="container flex-col flex-center-all auth-form">
    <h1>The best way to predict the future is to create it.</h1>
    <p>What are you waiting for? Join now.</p>
    <?= $this->Flash->render() ?>
    <div class="form-container flex-col">
        <?= $this->Form->create() ?>

        <?= $this->Form->control('email', [
            'label' => 'Email:',
            'required' => true,
            'placeholder' => 'Email Address...'
        ]) ?>
        <?= $this->Form->control('password', [
            'label' => 'Password:',
            'required' => true,
            'placeholder' => 'Password'
        ]) ?>

        <?= $this->Html->link("Don't have an account yet?", ['action' => 'add'], ['class' => 'special-link']) ?>
        <?= $this->Form->submit(__('Login'), ['class' => 'btn']); ?>
        <?= $this->Form->end() ?>
    </div>

</div>