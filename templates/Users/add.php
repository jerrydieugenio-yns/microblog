<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>



<div class="container flex-col flex-center-all signup-form auth-form">
    <h1>Share thoughts to the World</h1>
    <p>Join now, It's free.</p>
    <?= $this->Flash->render() ?>
    <div class="form-container flex-col">
        <?= $this->Form->create($user) ?>
        <?php
        echo $this->Form->control('display_name', [
            'label' => 'Display Name:',
            'placeholder' => 'Display Name'
        ]);
        echo $this->Form->control('email', [
            'label' => 'Email:',
            'placeholder' => 'Email Address'
        ]);
        echo $this->Form->control('username', [
            'label' => 'Username:',
            'placeholder' => 'Username'
        ]);
        echo $this->Form->control('password', [
            'label' => 'Password:',
            'placeholder' => 'Password'
        ]);
        echo $this->Form->control('retype_password', [
            'type' => 'password',
            'label' => 'Confirm Password:',
            'placeholder' => 'Retype Password'
        ]);
        ?>
        <?= $this->Html->link("Already have an account?", ['action' => 'login'], ['class' => 'special-link']) ?>
        <?= $this->Form->submit(__('Sign Up'), ['class' => 'btn']); ?>
        <?= $this->Form->end() ?>

    </div>
</div>