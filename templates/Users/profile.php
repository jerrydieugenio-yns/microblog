<section class="feed">
    <div class="feed-header">
        <h2>Profile</h2>
        <span class="text-status"><?= h(count($all_user_post)) ?> Posts</span>
    </div>
    <?= $this->Flash->render() ?>
    <div class="profile flex-col">
        <div class="profile-avatar">
            <img class="round" width="150" height="150" avatar="<?= h($user->display_name) ?>">
        </div>
        <h2><?= h($user->display_name) ?></h2>
        <span class="post-username">
            <h3>@<?= h($user->username) ?></h3>
        </span>
        <div class="flex-row flex-between">
            <div>
                <span class="date-joined">Joined <?= h($user->created->format('F Y')) ?></span>
                <div class="flex-row">
                    <a href="/users/following/<?= h($user->id) ?>" class="following"><?= h(count($following)) ?> <span class="text-status">following</span></a>
                    <a href="/users/followers/<?= h($user->id) ?>" class="followers"><?= h(count($followers)) ?> <span class="text-status">followers</span></a>
                </div>
            </div>
            <div>


                <?php
                if ($title === "user_profile") {
                    echo '<a href="/users/edit/' . h($user->id) . '" class="btn edit-profile-btn">Edit Profile</a>';
                } else {
                    if ($is_followed) {
                        echo '<a class="btn-followed" href="/users/unfollow/' . $user->id . '"><span>Following</span></a>';
                    } else {
                        echo $this->Html->link(
                            'Follow',
                            [
                                'action' => 'follow',
                                $user->id,
                            ],
                            ['class' => 'btn-unfollowed']
                        );
                    }
                }


                ?>
            </div>
        </div>
    </div>
    <h2 class="header-text">Latest Posts</h2>

    <?php

    $all_liked_posts = [];
    foreach ($liked_posts as $row) {
        $like_code = "";
        if ($row->is_retweet === true) {
            $like_code = "R";
        }
        array_push($all_liked_posts, $like_code . $row->post_id);
    }

    ?>
    <?php if (count($all_user_post) > 0) : ?>
        <?php


        $all_liked_posts = [];
        foreach ($liked_posts as $row) {
            $like_code = "";
            if ($row->is_retweet === true) {
                $like_code = "R";
            }
            array_push($all_liked_posts, $like_code . $row->post_id);
        }


        foreach ($all_user_post as $post) :
        ?>


            <?php if ($post->is_retweet === true) : ?>

                <div class="retweeted post-item" onclick="viewPost(<?= h($post->id) ?>, 1)">

                    <div class="retweeter">
                        <div class="post-avatar">
                            <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
                        </div>
                        <div class="post-author flex-row">
                            <h3>
                                <a href="/users/profile/<?= h($post->user_id) ?>" class="user-display-name" title="Visit Profile">
                                    <?= h($post->user['display_name']) ?>
                                </a>
                                <a href="/users/profile/<?= h($post->user_id) ?>" class="post-username" title="Visit Profile">
                                    @<?= h($post->user['username']) ?>
                                </a>
                                <span class="post-time">
                                    <?php
                                    $retweet_time = $post->created;

                                    echo h($retweet_time->timeAgoInWords([
                                        'accuracy' => [
                                            'year' => 'year',
                                            'month' => 'month',
                                            'week' => 'day',
                                            'day' => 'day',
                                            'hour' => 'hour',
                                            'minute' => 'minute',
                                            'second' => 'second'
                                        ]
                                    ]));
                                    ?>
                                </span>
                            </h3>
                            <span class="post-menu dropdown-icon" onclick="stopPropagation(event)">
                                <label class="dropdown">
                                    <span class="dd-button">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </span>
                                    <input type="checkbox" class="dd-input" id="test">

                                    <ul class="dd-menu">
                                        <?php if ($post->user_id == $user_logged_in->id) : ?>
                                            <li>
                                                <?= $this->Html->link(__('Edit Post'), ['action' => 'edit', 'controller' => 'Posts', h($post->id), h($post->is_retweet)]) ?>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <?= $this->Form->postLink(__('Delete Post'), ['action' => 'delete', 'controller' => 'Posts', h($post->id), h($post->is_retweet)], ['confirm' => __('Are you sure you want to delete # {0}?', h($post->id))]) ?>
                                            </li>
                                        <?php else : ?>
                                            <li>
                                                Hide Post
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Mute User
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Report Post
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Report User
                                            </li>
                                        <?php endif; ?>
                                    </ul>

                                </label>
                            </span>
                        </div>
                    </div>

                    <div class="retweeted-message">
                        <?= h($post->retweet_message) ?>
                    </div>

                    <div class="retweeted-post" onclick="viewPost(<?= h($post->id) ?>)">

                        <div class="post-avatar">
                            <img class="round" width="25" height="25" avatar="<?= h($post->post->user['display_name']) ?>">
                        </div>
                        <div class="post-body">

                            <div class="post-author flex-row">
                                <h3><a href="/users/profile/<?= h($post->post->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($post->post->user['display_name']) ?></a>
                                    <a href="/users/profile/<?= h($post->post->user_id) ?>" class="post-username" title="Visit Profile">
                                        @<?= h($post->post->user['username']) ?>
                                    </a>
                                    <div>
                                        <span class="text-status">
                                            <small><?= $post->post->created->i18nFormat() ?></small>
                                        </span>
                                        <span class="post-time">
                                            <small>
                                                <?php
                                                echo h($post->post->created->timeAgoInWords([
                                                    'accuracy' => [
                                                        'year' => 'year',
                                                        'month' => 'month',
                                                        'week' => 'day',
                                                        'day' => 'day',
                                                        'hour' => 'hour',
                                                        'minute' => 'minute',
                                                        'second' => 'second'
                                                    ]
                                                ]));
                                                ?>
                                            </small>
                                        </span>


                                    </div>
                                </h3>
                            </div>

                            <div class="post-header-description">
                                <p>
                                    <?= h($post->post->content) ?>
                                </p>

                            </div>
                            <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image', 'class' => 'post-image']) ?>



                        </div>
                    </div>
                    <div class="post-footer">

                        <?php if (in_array("R" . $post->id, $all_liked_posts)) : ?>
                            <a class="active" href="/posts/unlike/<?= h($post->id) ?>/1" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                        <?php else : ?>
                            <a href="/posts/like/<?= h($post->id) ?>/1" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                        <?php endif; ?>

                        <a href="/posts/view/<?= h($post->id) ?>/1" onclick="stopPropagation(event)"> <span><i class="far fa-comment"></i> <?= h(count($post->comments)) ?></span></a>
                        <a href="/posts/retweet/<?= h($post->post_id) ?>" onclick="stopPropagation(event)"><span><i class="fas fa-retweet"></i> </span></a>
                    </div>


                </div>


            <?php else : ?>



                <div class="post post-item" onclick="viewPost(<?= h($post->id) ?>)">

                    <div class="post-avatar">
                        <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
                    </div>
                    <div class="post-body">

                        <div class="post-author flex-row">
                            <h3><a href="/users/profile/<?= h($post->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($post->user['display_name']) ?></a>
                                <a href="/users/profile/<?= h($post->user_id) ?>" class="post-username" title="Visit Profile">
                                    @<?= h($post->user['username']) ?>
                                </a>
                                <div>
                                    <span class="text-status">
                                        <small><?= $post->created->i18nFormat() ?></small>
                                    </span>
                                    <span class="post-time">
                                        <small>
                                            <?php
                                            echo h($post->created->timeAgoInWords([
                                                'accuracy' => [
                                                    'year' => 'year',
                                                    'month' => 'month',
                                                    'week' => 'day',
                                                    'day' => 'day',
                                                    'hour' => 'hour',
                                                    'minute' => 'minute',
                                                    'second' => 'second'
                                                ]
                                            ]));
                                            ?>
                                        </small>
                                    </span>


                                </div>
                            </h3>
                            <span class="post-menu dropdown-icon" onclick="stopPropagation(event)">
                                <label class="dropdown">
                                    <span class="dd-button">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </span>
                                    <input type="checkbox" class="dd-input" id="test">
                                    <ul class="dd-menu">
                                        <?php if ($post->user['id'] == $user_logged_in->id) : ?>
                                            <li>
                                                <?= $this->Html->link(__('Edit Post'), ['action' => 'edit', 'controller' => 'Posts', h($post->id)],) ?>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <?= $this->Form->postLink(__('Delete Post'), ['action' => 'delete', 'controller' => 'Posts', h($post->id)], ['confirm' => __('Are you sure you want to delete # {0}?', h($post->id))]) ?>
                                            </li>
                                        <?php else : ?>
                                            <li>
                                                Hide Post
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Mute User
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Report Post
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                Report User
                                            </li>
                                        <?php endif; ?>
                                    </ul>

                                </label>
                            </span>
                        </div>

                        <div class="post-header-description">
                            <p>
                                <?= h($post->content) ?>
                            </p>

                        </div>
                        <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image']) ?>

                        <div class="post-footer">

                            <?php if (in_array($post->id, $all_liked_posts)) : ?>
                                <a class="active" href="/posts/unlike/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                            <?php else : ?>
                                <a href="/posts/like/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                            <?php endif; ?>


                            <a href="/posts/view/<?= h($post->id) ?>" onclick="stopPropagation(event)"> <span><i class="far fa-comment"></i> <?= h(count($post->comments)) ?></span></a>
                            <a href="/posts/retweet/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="fas fa-retweet"></i> <?= h(count($post->retweets)) ?></span></a>
                        </div>

                    </div>
                </div>
            <?php endif; ?>



        <?php endforeach; ?>
    <?php else : ?>
        <h2 class="text-center">No posts yet</h2>
    <?php endif; ?>

</section>

<section class="widgets">
    <div class="widgets-input">
        <i class="fas fa-search search-icon"></i>
        <?php
        echo $this->Form->create(null, [
            'type' => 'get',
            'url' => '/explore/users',
        ]);
        ?>
        <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

        <?= $this->Form->end() ?>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Trends for you</h2>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Suggested Users</h2>
    </div>

</section>


<script>
    const viewPost = (id, is_retweet = "") => {
        //Visit a post when clicking post body.
        location.href = "/posts/view/" + id + "/" + is_retweet;
    };

    const stopPropagation = (event) => {
        event.stopPropagation();
    };
</script>