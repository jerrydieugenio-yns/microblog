<div class="container flex-col flex-center-all verify-email-card">
    <div class="flex-col flex-center-all border">
        <i class="far fa-envelope"></i>
        <?= $this->Flash->render() ?>
        <h1>And oh, just one more step...</h1>
        <p>Activate your account and start sharing thoughts with amazing people.</p>
        <?= $this->Form->create($post, ['url' => ['action' => 'verify']]) ?>

        <?= $this->Form->control('id', ['type' => 'hidden', 'value' => $this->request->getQuery('id')]) ?>
        <?= $this->Form->button(__('Send Verification Email'), ['class' => 'btn']) ?>

        <?= $this->Form->end() ?>


    </div>
</div>