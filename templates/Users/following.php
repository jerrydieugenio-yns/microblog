<section class="feed">
    <div class="feed-header">
        <h2>Profile</h2>
        <span class="text-status">30 posts</span>
    </div>
    <?= $this->Flash->render() ?>
    <div class="profile flex-col">
        <div class="profile-avatar">
            <img class="round" width="150" height="150" avatar=" <?= h($user_profile->display_name) ?>">
        </div>
        <h2><?= h($user_profile->display_name) ?></h2>
        <span class="post-username">
            <h3>@<?= h($user_profile->username) ?></h3>
        </span>
        <div class="flex-row flex-between">
            <div>
                <span class="date-joined">Joined <?= h($user_profile->created->format('F Y')) ?></span>
                <div class="flex-row">
                    <a href="/users/following/<?= h($user_profile->id) ?>" class="following"><?= h(count($following)) ?> <span class="text-status">following</span></a>
                    <a href="/users/followers/<?= h($user_profile->id) ?>" class="followers"><?= h(count($followers)) ?> <span class="text-status">followers</span></a>
                </div>
            </div>
            <div>
                <div>
                    <?php
                    if ($title == "user_profile") {
                        echo '<a href="/users/edit/' . h($user_profile->id) . '" class="btn edit-profile-btn">Edit Profile</a>';
                    } else {
                        if ($is_followed) {
                            echo '<a class="btn-followed" href="/users/unfollow/' . $user_profile->id . '"><span>Following</span></a>';
                        } else {
                            echo $this->Html->link(
                                'Follow',
                                [
                                    'action' => 'follow',
                                    $user_profile->id,
                                ],
                                ['class' => 'btn-unfollowed']
                            );
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <h2 class="header-text">Following</h2>
    <?php if (sizeof($following)) : ?>
        <?php foreach ($following as $follower) : ?>
            <div class="user-card flex-row flex-between">
                <div class="flex-row flex-center-all">
                    <div class="user-avatar">
                        <img class="round" width="40" height="40" avatar="<?= h($follower->display_name) ?>">

                    </div>
                    <a href="/users/profile/<?= h($follower->id) ?>" class="explore-text user-display-name">
                        <h3 class="flex-col"> <?= h($follower->display_name) ?>
                            <span class="post-username">
                                @<?= h($follower->username) ?>
                            </span>
                        </h3>
                    </a>
                </div>
                <div class="flex-center-all">
                    <?php
                    if ($user_logged_in->id == $follower->id) {
                        //
                    } else if ($follower->id) {
                        echo '<a class="btn-followed" href="/users/unfollow/' . $follower->id . '"><span>Following</span></a>';
                    } else {
                        echo $this->Html->link(
                            'Follow',
                            [
                                'action' => 'follow',
                                $follower->id,
                            ],
                            ['class' => 'btn-unfollowed']
                        );
                    }
                    ?>
                </div>
            </div>
        <?php endforeach; ?>

    <?php else : ?>
        <h2 class="text-center">No followed user yet</h2>
    <?php endif; ?>
</section>