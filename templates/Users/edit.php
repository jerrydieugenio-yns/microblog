<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>




<section class="feed">
    <div class="feed-header">
        <h2>Edit Profile</h2>
    </div>
    <?= $this->Form->create($user) ?>
    <div class="edit-profile flex-col">
        <div class="profile-avatar">
            <img class="round" width="150" height="150" avatar="<?= h($user->display_name) ?>">
        </div>
        <div class="upload">
            <label class="custom-file-upload">
                <input type="file" />
                Change
            </label>
        </div>
        <?= $this->Flash->render() ?>
        <?php
        echo '<small class="text-left"><strong>Personal Info</strong></small>';
        echo $this->Form->control('display_name', [
            'label' => false,
            'placeholder' => 'Display Name'
        ]);
        echo $this->Form->control('email', [
            'label' => false,
            'placeholder' => 'Email Address'
        ]);
        echo $this->Form->control('username', [
            'label' => false,
            'placeholder' => 'Username'
        ]);
        echo '<small class="text-left"><strong>Change Password</strong></small>';
        echo $this->Form->control('old_password', [
            'type' => 'password',
            'label' => false,
            'placeholder' => 'Old Password',
            'value' => ''
        ]);
        echo $this->Form->control('new_password', [
            'type' => 'password',
            'label' => false,
            'placeholder' => 'New Password',
            'value' => ''
        ]);
        echo $this->Form->control('retype_new_password', [
            'type' => 'password',
            'label' => false,
            'placeholder' => 'Retype New Password'
        ]);

        echo $this->Form->button(__('Save Changes'), ['class' => 'btn'])
        ?>
    </div>
    <?= $this->Form->end() ?>


</section>