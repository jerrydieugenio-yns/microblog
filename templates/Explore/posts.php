<section class="feed">
    <div class="feed-header">
        <div class="widgets-input">
            <i class="fas fa-search search-icon"></i>
            <?php
            echo $this->Form->create(null, [
                'type' => 'get',
                'controller' => 'explore',
                'action' => 'users'
            ]);
            ?>
            <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

            <?= $this->Form->end() ?>
        </div>
    </div>

    <?= $this->Flash->render() ?>



    <h2 class="explore-text">Featured Posts [Sample]</h2>
    <div class="post">
        <div class="post-avatar">
            <img class="round" width="40" height="40" avatar="John Doe">
        </div>
        <div class="post-body">

            <div class="post-author flex-row">
                <h3>John Doe
                    <span class="post-username">
                        @johndoe
                    </span>
                    <span class="post-time">
                        10mins ago
                    </span>
                </h3>
                <span class="post-menu dropdown-icon">
                    <i class="fas fa-ellipsis-h"></i>
                </span>
            </div>

            <div class="post-header-description">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quidem corporis, maxime quod mollitia consectetur libero facere deleniti hic saepe blanditiis dolorum dolor cum reprehenderit ducimus quae facilis officia error.
                </p>
            </div>
            <img src="images/sample-image.jpg" alt="">

            <div class="post-footer">
                <span><i class="far fa-thumbs-up"></i> 12k</span>
                <span><i class="far fa-comment"></i> 139</span>
                <span><i class="fas fa-retweet"></i> 1.2k</span>
            </div>

        </div>
    </div>
</section>
<section class="widgets">
    <h2 class="explore-text">Search Results</h2>
    <a href="/explore">
        <div class="filter-wrapper">
            <h2><i class="far fa-newspaper filter-icon"></i>All</h2>
        </div>
    </a>
    <a href="/explore/users">
        <div class="filter-wrapper">
            <h2><i class="fas fa-users filter-icon"></i>Users</h2>
        </div>
    </a>
    <a href="/explore/users">
        <div class="filter-wrapper active">
            <h2><i class="fas fa-edit filter-icon"></i>Posts</h2>
        </div>
    </a>

</section>