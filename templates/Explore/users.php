<section class="feed">
    <div class="feed-header">
        <div class="widgets-input">
            <i class="fas fa-search search-icon"></i>
            <?php
            echo $this->Form->create(null, [
                'type' => 'get'
            ]);
            ?>
            <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

            <?= $this->Form->end() ?>
        </div>
    </div>

    <?= $this->Flash->render() ?>










    <h2 class="explore-text">Users you might know</h2>

    <?php foreach ($users as $user) :
        // debug($user);
    ?>
        <div class="user-card flex-row flex-between">
            <div class="flex-row flex-center-all">
                <div class="user-avatar">
                    <img class="round" width="40" height="40" avatar="<?= h($user->display_name) ?>">
                </div>
                <h3 class="flex-col"><a href="/users/profile/<?= h($user->id) ?>" class="user-display-name" title="Visit Profile"><?= h($user->display_name) ?></a>
                    <a href="/users/profile/<?= h($user->id) ?>" class="post-username" title="Visit Profile">
                        @<?= h($user->username) ?>
                    </a>
                </h3>
            </div>
            <div class="flex-center-all">
                <?php


                $all_followed_users = [];
                foreach ($followed_users as $row) {
                    array_push($all_followed_users, $row->followed_user_id);
                }

                ?>
                <?php if (in_array($user->id, $all_followed_users)) : ?>

                    <a class="btn-followed" href="/users/unfollow/<?= h($user->id) ?>"><span>Following</span></a>
                <?php elseif ($user->id === $user_logged_in->id) : ?>
                <?php else : ?>
                    <a class="btn-unfollowed" href="/users/follow/<?= h($user->id) ?>"><span>Follow</span></a>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?php
    $total_records = count($users);
    $added_record = 3;
    $current = $this->request->getQuery('limit') ?? 3;
    ?>

    <?php if (isset($current) && $current > $total_records) : ?>
        <br>
        <p class='text-center'>No more records to show.</p>
        <br>
    <?php else : ?>
        <div class='text-center'>
            <a href="?limit=<?= $current +  $added_record ?>">More...</a>
        </div>
    <?php endif; ?>

</section>
<section class="widgets">
    <h2 class="explore-text">Search Results</h2>
    <a href="/explore">
        <div class="filter-wrapper">
            <h2><i class="far fa-newspaper filter-icon"></i>All</h2>
        </div>
    </a>
    <a href="/explore/users">
        <div class="filter-wrapper active">
            <h2><i class="fas fa-users filter-icon"></i>Users</h2>
        </div>
    </a>
    <a href="/explore/posts">
        <div class="filter-wrapper">
            <h2><i class="fas fa-edit filter-icon"></i>Posts</h2>
        </div>
    </a>

</section>