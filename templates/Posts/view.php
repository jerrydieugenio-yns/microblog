<section class="feed post-page">
    <div class="feed-header">
        <h2>Post</h2>
    </div>
    <?= $this->Flash->render() ?>

    <?php

    use Cake\I18n\FrozenTime;

    $all_liked_posts = [];
    foreach ($liked_posts as $row) {
        $like_code = "";
        if ($row->is_retweet === true) {
            $like_code = "R";
        }
        array_push($all_liked_posts, $like_code . $row->post_id);
    }
    ?>

    <?php if ($post->is_retweet === true) : ?>

        <div class="retweeted" onclick="viewPost(<?= h($post->id) ?>, 1)">

            <div class="retweeter">
                <div class="post-avatar">
                    <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
                </div>
                <div class="post-author flex-row">
                    <h3>
                        <a href="/users/profile/<?= h($post->user_id) ?>" class="user-display-name" title="Visit Profile">
                            <?= h($post->user['display_name']) ?>
                        </a>
                        <a href="/users/profile/<?= h($post->user_id) ?>" class="post-username" title="Visit Profile">
                            @<?= h($post->user['username']) ?>
                        </a>
                        <span class="post-time">
                            <?php
                            $retweet_time = new FrozenTime($post->created);

                            echo h($retweet_time->timeAgoInWords([
                                'accuracy' => [
                                    'year' => 'year',
                                    'month' => 'month',
                                    'week' => 'day',
                                    'day' => 'day',
                                    'hour' => 'hour',
                                    'minute' => 'minute',
                                    'second' => 'second'
                                ]
                            ]));
                            ?>
                        </span>
                    </h3>
                    <span class="post-menu dropdown-icon" onclick="stopPropagation(event)">
                        <label class="dropdown">
                            <span class="dd-button">
                                <i class="fas fa-ellipsis-h"></i>
                            </span>
                            <input type="checkbox" class="dd-input" id="test">

                            <ul class="dd-menu">
                                <?php if ($post->user_id == $user_logged_in->id) : ?>
                                    <li>
                                        <?= $this->Html->link(__('Edit Post'), ['action' => 'edit', h($post->id), h($post->is_retweet)]) ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->Form->postLink(__('Delete Post'), ['action' => 'delete', h($post->id), h($post->is_retweet)], ['confirm' => __('Are you sure you want to delete # {0}?', h($post->id))]) ?>
                                    </li>
                                <?php else : ?>
                                    <li>
                                        Hide Post
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Mute User
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Report Post
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Report User
                                    </li>
                                <?php endif; ?>
                            </ul>

                        </label>
                    </span>
                </div>
            </div>

            <div class="retweeted-message">
                <?= h($post->retweet_message) ?>
            </div>

            <div class="retweeted-post" onclick="viewPost(<?= h($post->id) ?>)">

                <div class="post-avatar">
                    <img class="round" width="40" height="40" avatar="<?= h($post->post->user['display_name']) ?>">
                </div>
                <div class="post-body">

                    <div class="post-author flex-row">
                        <h3><a href="/users/profile/<?= h($post->post->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($post->post->user['display_name']) ?></a>
                            <a href="/users/profile/<?= h($post->post->user_id) ?>" class="post-username" title="Visit Profile">
                                @<?= h($post->post->user['username']) ?>
                            </a>
                            <div>
                                <span class="text-status">
                                    <small><?= $post->created->i18nFormat() ?></small>
                                </span>
                                <span class="post-time">
                                    <small>
                                        <?php
                                        echo h($post->created->timeAgoInWords([
                                            'accuracy' => [
                                                'year' => 'year',
                                                'month' => 'month',
                                                'week' => 'day',
                                                'day' => 'day',
                                                'hour' => 'hour',
                                                'minute' => 'minute',
                                                'second' => 'second'
                                            ]
                                        ]));
                                        ?>
                                    </small>
                                </span>


                            </div>
                        </h3>
                    </div>

                    <div class="post-header-description">
                        <p>
                            <?= h($post->post->content) ?>
                        </p>

                    </div>
                    <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image', 'class' => 'post-image']) ?>



                </div>
            </div>
            <div class="post-footer">

                <?php if (in_array("R" . $post->id, $all_liked_posts)) : ?>
                    <a class="active" href="/posts/unlike/<?= h($post->id) ?>/1" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                <?php else : ?>
                    <a href="/posts/like/<?= h($post->id) ?>/1" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                <?php endif; ?>

                <a href="/posts/view/<?= h($post->id) ?>" onclick="stopPropagation(event)"> <span><i class="far fa-comment"></i> <?= h(count($post->comments)) ?></span></a>
                <a href="/posts/retweet/<?= h($post->post_id) ?>" onclick="stopPropagation(event)"><span><i class="fas fa-retweet"></i> </span></a>
            </div>


        </div>


    <?php else : ?>
        <div class="post view-post">
            <div class="post-avatar">
                <img class="round" width="50" height="50" avatar="<?= h($post->user->display_name) ?>">
            </div>
            <div class="post-body">

                <div class="post-author flex-row">
                    <h3><a href="/users/profile/<?= h($post->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($post->user->display_name) ?></a>
                        <a href="/users/profile/<?= h($post->user_id) ?>" class="post-username" title="Visit Profile">
                            @<?= h($post->user->username) ?>
                        </a>

                    </h3>
                    <span class="post-menu dropdown-icon" onclick="stopPropagation(event)">
                        <label class="dropdown">
                            <span class="dd-button">
                                <i class="fas fa-ellipsis-h"></i>
                            </span>
                            <input type="checkbox" class="dd-input" id="test">

                            <ul class="dd-menu">
                                <?php if ($post->user_id == $user_logged_in->id) : ?>
                                    <li>
                                        <?= $this->Html->link(__('Edit Post'), ['action' => 'edit', h($post->id), h($post->is_retweet)]) ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?= $this->Form->postLink(__('Delete Post'), ['action' => 'delete', h($post->id), h($post->is_retweet)], ['confirm' => __('Are you sure you want to delete # {0}?', h($post->id))]) ?>
                                    </li>
                                <?php else : ?>
                                    <li>
                                        Hide Post
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Mute User
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Report Post
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        Report User
                                    </li>
                                <?php endif; ?>
                            </ul>

                        </label>
                    </span>
                </div>

                <div class="post-header-description">
                    <p><?= h($post->content) ?></p>
                </div>
                <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image', 'class' => 'post-image']) ?>
                <div>
                    <span class="text-status">
                        <small><?= h($post->created->i18nFormat()) ?></small>
                    </span>
                    <span class="post-time">
                        <small>
                            <?php
                            echo h($post->created->timeAgoInWords([
                                'accuracy' => [
                                    'year' => 'year',
                                    'month' => 'month',
                                    'week' => 'day',
                                    'day' => 'day',
                                    'hour' => 'hour',
                                    'minute' => 'minute',
                                    'second' => 'second'
                                ]
                            ]));
                            ?>
                        </small>
                    </span>

                </div>
                <div class="post-footer">

                    <?php if (in_array($post->id, $all_liked_posts)) : ?>
                        <a class="active" href="/posts/unlike/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                    <?php else : ?>
                        <a href="/posts/like/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="far fa-thumbs-up"></i> <?= h(count($post->likes)) ?></span></a>
                    <?php endif; ?>


                    <a href="/posts/view/<?= h($post->id) ?>" onclick="stopPropagation(event)"> <span><i class="far fa-comment"></i> <?= h(count($post->comments)) ?></span></a>
                    <a href="/posts/retweet/<?= h($post->id) ?>" onclick="stopPropagation(event)"><span><i class="fas fa-retweet"></i> <?= h(count($post->retweets)) ?></span></a>
                </div>

            </div>
        </div>

    <?php endif; ?>
    <div class="commentbox flex-col">
        <h2>Comments</h2>
        <?= $this->Form->create($post, ['url' => ['action' => 'comment']]) ?>
        <div class="commentbox-input">

            <div class="commentbox-avatar">
                <img class="round" width="40" height="40" avatar="<?= h($user_logged_in->display_name) ?>">
            </div>

            <?php
            echo $this->Form->control('id', ['type' => 'hidden']);
            echo $this->Form->control('is_retweet', ['type' => 'hidden']);
            ?>
            <?= $this->Form->textarea('comment', ['type' => 'textarea', 'placeholder' => "Comment..."]) ?>
            <?= $this->Form->button(__('Comment'), ['class' => 'comment-button']) ?>


        </div>
        <?= $this->Form->end() ?>


        <?php foreach ($post->comments as $comment) : ?>
            <div class="comment">
                <div class="flex-row flex-align-center user-commented">
                    <div>
                        <img class="round" width="40" height="40" avatar="<?= h($comment->user['display_name']) ?>">
                    </div>
                    <h3>
                        <a href="/users/profile/<?= h($comment->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($comment->user['display_name']) ?></a>
                        <a href="/users/profile/<?= h($comment->user_id) ?>" class="user-display-name comment-username" title="Visit Profile">
                            @<?= h($comment->user['username']) ?>
                        </a>
                        <span class="comment-time"> <?= h($comment->created) ?> </span>
                    </h3>
                </div>
                <div class="content">
                    <?= h($comment->comment) ?>
                </div>
            </div>
        <?php endforeach; ?>
        <?php
        $total_records = count($post->comments);
        $added_record = 3;
        $current = $this->request->getQuery('limit') ?? 3;
        ?>

        <?php if (isset($current) && $current > $total_records) : ?>
            <br>
            <p class='text-center'>No more comments to show.</p>
            <br>
        <?php else : ?>
            <div class='text-center'>
                <a href="?limit=<?= $current +  $added_record ?>">More...</a>
            </div>
        <?php endif; ?>


    </div>




</section>
<section class="widgets">
    <div class="widgets-input">
        <i class="fas fa-search search-icon"></i>
        <?php
        echo $this->Form->create(null, [
            'type' => 'get',
            'url' => '/explore/users',
        ]);
        ?>
        <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

        <?= $this->Form->end() ?>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Trends for you</h2>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Suggested Users</h2>
    </div>

</section>