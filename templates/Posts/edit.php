<!-- File: templates/Posts/edit.php -->



<section class="feed post-page">
    <div class="feed-header">
        <h2>Edit Post</h2>
    </div>


    <div class="createbox">
        <?php
        echo $this->Form->create($post);
        echo $this->Form->control('user_id', ['type' => 'hidden']);
        ?>
        <div class="createbox-input">
            <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
            <?= $this->Form->textarea('content', ['type' => 'textarea', 'placeholder' => "What's happening?", 'value' => ($post->is_retweet === true) ? $post->retweet_message : $post->content]) ?>
        </div>
        <div class="createbox-menu flex-row">

            <div class="image-upload">
                <label for="file-input">
                    <i class="far fa-image"></i>
                </label>

                <input id="file-input" type="file" />
            </div>
            <?= $this->Form->button(__('Update Post'), ['class' => 'createbox-post-button']) ?>
        </div>
        <?= $this->Form->end() ?>

    </div>
    <?= $this->Flash->render() ?>

    <h2 class="explore-text text-status">Post Preview</h2>
    <?php

    use Cake\I18n\FrozenTime;

    ?>

    <?php if ($post->is_retweet === true) : ?>

        <div class="retweeted editing-preview" onclick="viewPost(<?= h($post->id) ?>, 1)">

            <div class="retweeter">
                <div class="post-avatar">
                    <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
                </div>
                <div class="post-author flex-row">
                    <h3>
                        <a href="/users/profile/<?= h($post->user_id) ?>" class="user-display-name" title="Visit Profile">
                            <?= h($post->user['display_name']) ?>
                        </a>
                        <a href="/users/profile/<?= h($post->user_id) ?>" class="post-username" title="Visit Profile">
                            @<?= h($post->user['username']) ?>
                        </a>
                        <span class="post-time">
                            <?php
                            $retweet_time = new FrozenTime($post->created);

                            echo h($retweet_time->timeAgoInWords([
                                'accuracy' => [
                                    'year' => 'year',
                                    'month' => 'month',
                                    'week' => 'day',
                                    'day' => 'day',
                                    'hour' => 'hour',
                                    'minute' => 'minute',
                                    'second' => 'second'
                                ]
                            ]));
                            ?>
                        </span>
                    </h3>

                </div>
            </div>

            <div class="retweeted-message">
                <?= h($post->retweet_message) ?>
            </div>

            <div class="retweeted-post" onclick="viewPost(<?= h($post->id) ?>)">

                <div class="post-avatar">
                    <img class="round" width="25" height="25" avatar="<?= h($post->post->user['display_name']) ?>">
                </div>
                <div class="post-body">

                    <div class="post-author flex-row">
                        <h3><a href="/users/profile/<?= h($post->post->user_id) ?>" class="user-display-name" title="Visit Profile"> <?= h($post->post->user['display_name']) ?></a>
                            <a href="/users/profile/<?= h($post->post->user_id) ?>" class="post-username" title="Visit Profile">
                                @<?= h($post->post->user['username']) ?>
                            </a>
                            <div>
                                <span class="text-status">
                                    <small><?= $post->created->i18nFormat() ?></small>
                                </span>
                                <span class="post-time">
                                    <small>
                                        <?php
                                        echo h($post->created->timeAgoInWords([
                                            'accuracy' => [
                                                'year' => 'year',
                                                'month' => 'month',
                                                'week' => 'day',
                                                'day' => 'day',
                                                'hour' => 'hour',
                                                'minute' => 'minute',
                                                'second' => 'second'
                                            ]
                                        ]));
                                        ?>
                                    </small>
                                </span>


                            </div>
                        </h3>
                    </div>

                    <div class="post-header-description">
                        <p>
                            <?= h($post->post->content) ?>
                        </p>

                    </div>
                    <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image', 'class' => 'post-image']) ?>



                </div>
            </div>


        </div>


    <?php else : ?>
        <div class="post view-post editing-preview">
            <div class="post-avatar">
                <img class="round" width="40" height="40" avatar="<?= h($post->user->display_name) ?>">
            </div>
            <div class="post-body">

                <div class="post-author flex-row">
                    <h3><?= h($post->user->display_name) ?>
                        <span class="post-username">
                            @<?= h($post->user->username) ?>
                        </span>
                    </h3>

                </div>

                <div class="post-header-description">
                    <p><?= h($post->content) ?></p>
                </div>
                <img src="images/sample-image.jpg" alt="">
                <div>
                    <span class="text-status">
                        <small><?= h($post->created->i18nFormat()) ?></small>
                    </span>
                    <span class="post-time">
                        <small>
                            <?php
                            echo h($post->created->timeAgoInWords([
                                'accuracy' => [
                                    'year' => 'year',
                                    'month' => 'month',
                                    'week' => 'day',
                                    'day' => 'day',
                                    'hour' => 'hour',
                                    'minute' => 'minute',
                                    'second' => 'second'
                                ]
                            ]));
                            ?>
                        </small>
                    </span>

                </div>

            </div>
        </div>

    <?php endif; ?>



</section>
<section class="widgets">
    <div class="widgets-input">
        <i class="fas fa-search search-icon"></i>
        <?php
        echo $this->Form->create(null, [
            'type' => 'get',
            'url' => '/explore/users',
        ]);
        ?>
        <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

        <?= $this->Form->end() ?>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Trends for you</h2>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Suggested Users</h2>
    </div>

</section>

<script>
    const viewPost = (id, is_retweet = "") => {
        //Visit a post when clicking post body.
        location.href = "/posts/view/" + id + "/" + is_retweet;
    };

    const stopPropagation = (event) => {
        event.stopPropagation();
    };
</script>