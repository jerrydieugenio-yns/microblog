<!-- File: templates/Posts/edit.php -->



<section class="feed post-page">
    <div class="feed-header">
        <h2>Retweet Post</h2>
    </div>


    <div class="createbox">
        <?php
        echo $this->Form->create();
        echo $this->Form->control('post_id', [
            'value' => $post->id,
            'type' => 'hidden'
        ]);
        ?>
        <div class="createbox-input">
            <img class="round" width="40" height="40" avatar="<?= h($post->user['display_name']) ?>">
            <?= $this->Form->textarea('retweet_message', ['type' => 'textarea', 'placeholder' => "What's happening?"]) ?>
        </div>
        <div class="createbox-menu flex-row">

            <div class="image-upload">
                <label for="file-input">
                    <i class="far fa-image"></i>
                </label>

                <input id="file-input" type="file" />
            </div>
            <?= $this->Form->button(__('Retweet Post'), ['class' => 'createbox-post-button']) ?>
        </div>
        <?= $this->Form->end() ?>

    </div>
    <?= $this->Flash->render() ?>

    <h2 class="explore-text text-status">Post Preview</h2>
    <div class="post editing-preview">

        <div class="post-avatar">
            <img class="round" width="40" height="40" avatar="<?= h($post->user->display_name) ?>">
        </div>
        <div class="post-body">

            <div class="post-author flex-row">
                <h3><?= h($post->user->display_name) ?>
                    <span class="post-username">
                        @<?= h($post->user->username) ?>
                    </span>
                </h3>

            </div>

            <div class="post-header-description">
                <p><?= h($post->content) ?></p>
            </div>
            <?= $this->Html->image('sample-image.jpg', ['alt' => 'Image']) ?>
            <div>
                <span class="post-time">
                    <?= h($post->created->i18nFormat()) ?>
                </span>
                <span class="post-date">
                    <?php
                    echo h($post->created->timeAgoInWords([
                        'accuracy' => [
                            'year' => 'year',
                            'month' => 'month',
                            'week' => 'day',
                            'day' => 'day',
                            'hour' => 'hour',
                            'minute' => 'minute',
                            'second' => 'second'
                        ]
                    ]));
                    ?>
                </span>
            </div>


        </div>
    </div>





</section>
<section class="widgets">
    <div class="widgets-input">
        <i class="fas fa-search search-icon"></i>
        <?php
        echo $this->Form->create(null, [
            'type' => 'get',
            'url' => '/explore/users',
        ]);
        ?>
        <?= $this->Form->control('key', ['placeholder' => 'Search Blogs, Users, Posts', 'label' => false, 'value' => $this->request->getQuery('key')]) ?>

        <?= $this->Form->end() ?>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Trends for you</h2>
    </div>
    <div class="widgets-wrapper">
        <h2 class="widget">Suggested Users</h2>
    </div>

</section>