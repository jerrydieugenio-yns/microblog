<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Microblog - Ideas For Everyone';
?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?> -
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('icon', 'https://icons-for-free.com/iconfiles/png/512/idea+idea+bulb+light+bulb+icon-1320144733751939202.png', ['type' => 'image/png']) ?>
    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet"> -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arimo&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>



    <?= $this->Html->css(['main', 'cake']) ?>
    <?= $this->Html->script(['letter-avatar']) ?>
    <!-- 'normalize.min', 'milligram.min', 'cake', -->


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <section class="sidebar">
        <div class="logo">
            <h2>Microblog</h2>
        </div>
        <a href="/" class="sidebar-option <?= ($title === "home") ? 'active' : '' ?>">
            <i class="fas fa-home sidebar-icons"></i>
            <h2>Home</h2>
        </a>
        <a href="/explore" class="sidebar-option">
            <i class="fas fa-search sidebar-icons"></i>
            <h2>Explore</h2>
        </a>
        <a href="#" class="sidebar-option">
            <i class="fas fa-bell sidebar-icons"></i>
            <h2>Notifications</h2>
        </a>
        <a href="/users/profile" class="sidebar-option <?= ($title === "profile") ? 'active' : '' ?>">
            <i class="fas fa-user sidebar-icons"></i>
            <h2>Profile</h2>
        </a>
        <a href="#" class="sidebar-option">
            <i class="fas fa-list sidebar-icons"></i>
            <h2>Activity</h2>
        </a>
        <a href="/users/logout" class="sidebar-option">
            <i class="fas fa-power-off sidebar-icons"></i>
            <h2>Sign Out</h2>
        </a>
        <button class="sidebar-post-btn" onclick="openModal('post_modal')">Post</button>

    </section>
    <?= $this->fetch('content') ?>





    <!-- modal -->
    <div class="overlay" id="overlay"></div>
    <div class="modal" id="post_modal">
        <button class="modal-close-btn" id="post_modalclose-btn"><i class="fa fa-times" title="閉じる"></i></button>
        <h2>Create Post</h2>
        <div class="createbox">
            <?= $this->Form->create($post, ['url' => ['controller' => 'Posts', 'action' => 'add']]) ?>
            <div class="createbox-input">
                <?= $this->Html->image('default-avatar.png', ['alt' => 'User Avatar']) ?>
                <!-- <textarea placeholder="What's happening?"></textarea> -->
                <?= $this->Form->textarea('content', ['type' => 'textarea', 'placeholder' => "What's happening?"]) ?>
            </div>
            <div class="createbox-menu flex-row">

                <div class="image-upload">
                    <label for="file-input">
                        <i class="far fa-image"></i>
                    </label>

                    <input id="file-input" type="file" />
                </div>
                <?= $this->Form->button(__('Post'), ['class' => 'createbox-post-button']) ?>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        const openModal = (modal_id) => {
            document.getElementById('overlay').classList.add('is-visible');
            document.getElementById(modal_id).classList.add('is-visible');
            document.getElementById(modal_id + 'close-btn').addEventListener('click', function() {
                document.getElementById('overlay').classList.remove('is-visible');
                document.getElementById(modal_id).classList.remove('is-visible');
            });
            document.getElementById('overlay').addEventListener('click', function() {
                document.getElementById('overlay').classList.remove('is-visible');
                document.getElementById(modal_id).classList.remove('is-visible');
            });
        };
    </script>

    <script>
        function autoGrow(oField) {
            if (oField.scrollHeight > oField.clientHeight) {
                oField.style.height = oField.scrollHeight + "px";
            }
        }
    </script>

</body>

</html>