<?php

namespace App\Policy;

use App\Model\Entity\Post;
use App\Model\Entity\Comment;
use Authorization\IdentityInterface;

class PostPolicy
{
    public function canIndex(IdentityInterface $user, Post $post)
    {
        // All logged in users can view posts
        return true;
    }
    public function canAdd(IdentityInterface $user, Post $post)
    {
        // All logged in users can view posts
        return true;
    }

    public function canEdit(IdentityInterface $user, Post $post)
    {
        // logged in users can edit their can view post
        return $this->isAuthor($user, $post);
    }

    public function canDelete(IdentityInterface $user, Post $post)
    {
        // logged in users can delete their can view posts
        return $this->isAuthor($user, $post);
    }

    public function canRetweet(IdentityInterface $user, Post $post)
    {
        return true;
    }
    public function canView(IdentityInterface $user, Post $post)
    {
        return true;
    }
    public function canLike(IdentityInterface $user, Post $post)
    {
        return true;
    }
    public function canUnlike(IdentityInterface $user, Post $post)
    {
        return true;
    }
    public function canComment(IdentityInterface $user, Post $post)
    {
        return true;
    }

    protected function isAuthor(IdentityInterface $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
