<?php

declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;


/**
 * User policy
 */
class UserPolicy
{
    public function canAdd(IdentityInterface $user, User $resource)
    {
        return false;
    }

    public function canEdit(IdentityInterface $user, User $resource)
    {
        return $this->isUser($user, $resource);
    }

    public function canDelete(IdentityInterface $user, User $resource)
    {
        return $this->isUser($user, $resource);
    }

    public function canView(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canProfile(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canFollow(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canUnfollow(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canFollowing(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canFollowers(IdentityInterface $user, User $resource)
    {
        return true;
    }
    public function canVerify(IdentityInterface $user, User $resource)
    {
        return true;
    }



    protected function isUser(IdentityInterface $user, User $resource)
    {
        return $user->id === $resource->id;
    }
}
