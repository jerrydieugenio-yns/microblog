<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;


class Comment extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => true,
        'user_id' => true,
        'post_id' => true,
    ];
}
