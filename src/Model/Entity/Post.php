<?php
// src/Model/Entity/Post.php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Post extends Entity
{
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'user_id' => true,
        'comments' => true,
        'likes' => true,
        'retweets' => true,
        'users' => true,
    ];
}
