<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use SoftDelete\Model\Table\SoftDeleteTrait;

class RetweetsTable extends Table
{
    use SoftDeleteTrait;
    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setTable('retweets');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
        $this->belongsTo('Posts')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
        $this->hasMany('Follows')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
        $this->hasMany('Likes')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
        $this->hasMany('Comments')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
    }
}
