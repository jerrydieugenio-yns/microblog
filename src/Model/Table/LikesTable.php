<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class LikesTable extends Table
{

    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setTable('likes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
        $this->belongsTo('Posts')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
    }
}
