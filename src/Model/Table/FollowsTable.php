<?php
// src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class FollowsTable extends Table
{

    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->setTable('follows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('Users')
            ->setForeignKey(['follower_user_id', 'followed_user_id'])
            ->setJoinType('INNER');
    }
}
