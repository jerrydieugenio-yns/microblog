<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CommentsTable extends Table
{

    public function initialize(array $config): void
    {

        // $this->addBehavior('Timestamp');
        // $this->belongsTo('Users')
        //     // ->setForeignKey(['follower_user_id', 'followed_user_id'])
        //     ->setForeignKey('user_id')
        //     ->setJoinType('INNER');
        // $this->belongsTo('Posts')
        //     // ->setForeignKey(['follower_user_id', 'followed_user_id'])
        //     ->setForeignKey('post_id')
        //     ->setJoinType('INNER');

        $this->addBehavior('Timestamp');
        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('LEFT');
        $this->belongsTo('Posts')
            ->setForeignKey('post_id')
            ->setJoinType('LEFT');
        $this->belongsTo('Retweets')
            ->setForeignKey('post_id')
            ->setJoinType('LEFT');
    }
}
