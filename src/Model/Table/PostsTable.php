<?php
// src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

class PostsTable extends Table
{
    use SoftDeleteTrait;
    public function initialize(array $config): void
    {

        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->belongsTo('Users')
            ->setForeignKey('user_id')
            ->setJoinType('INNER');
        $this->hasMany('Retweets')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
        $this->hasMany('Follows')
            ->setForeignKey('follower_user_id')
            ->setJoinType('INNER');
        $this->hasMany('Likes')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
        $this->hasMany('Comments')
            ->setForeignKey('post_id')
            ->setJoinType('INNER');
    }
    public function validationDefault(Validator $validator): Validator
    {
        return $validator
            ->notEmptyString('content', 'Posts must have atleast 2 to 140 characters.')
            ->minLength('content', 2)
            ->maxLength('content', 140);
    }
}
