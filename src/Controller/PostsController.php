<?php
// src/Controller/PostsController.php

namespace App\Controller;

use App\Controller\AppController;

class PostsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->set('title', 'posts');
        $this->viewBuilder()->setLayout('main');
    }
    public function index()
    {


        $this->loadComponent('Paginator');
        $this->loadModel('Retweets');
        $this->loadModel('Posts');
        $this->loadModel('Likes');
        $this->Authorization->skipAuthorization();
        $user_logged_in = $this->request->getAttribute('identity');

        $query = $this->Posts->find()
            ->select([
                'Posts.id',
                'post_id' => 'Posts.id',
                'Posts.content',
                'retweet_message' => 'Posts.content',
                'Posts.user_id',
                'created' => 'Posts.created',
                'post_created' => 'Posts.created',
                'Users.display_name',
                'Users.username',
                'retweeter.id',
                'retweeter.display_name',
                'retweeter.username',
                'Posts.is_retweet',
            ])
            ->contain(['Users', 'Likes', 'Comments', 'Retweets'])
            ->join(['table' => 'follows', 'type' => 'LEFT', 'conditions' => 'Posts.user_id = follows.followed_user_id AND follows.follower_user_id = :user_id'])
            ->join(['table' => 'users', 'alias' => 'retweeter', 'conditions' => 'Posts.user_id = retweeter.id'])
            ->order(['Posts.created' => 'DESC'])
            ->where(['follows.follower_user_id = :user_id OR Posts.user_id = :user_id'])
            ->bind(':user_id', $user_logged_in->id);


        $retweet_query = $this->Retweets->find()
            ->select([
                'Retweets.id',
                'post_id' => 'posts.id',
                'posts.content',
                'Retweets.retweet_message',
                'posts.user_id',
                'created' => 'Retweets.created',
                'post_created' => 'posts.created',
                'users.display_name',
                'users.username',
                'retweeter.id',
                'retweeter.display_name',
                'retweeter.username',
                'Retweets.is_retweet',
            ])
            ->contain(['Likes', 'Comments'])
            ->join(['table' => 'posts', 'type' => 'INNER', 'conditions' => 'posts.id = Retweets.post_id'])
            ->join(['table' => 'follows', 'type' => 'INNER', 'conditions' => 'Retweets.user_id = follows.followed_user_id AND follows.follower_user_id = :user_id OR Retweets.user_id = :user_id'])
            ->join(['table' => 'users', 'conditions' => 'users.id = posts.user_id'])
            ->join(['table' => 'users', 'alias' => 'retweeter', 'conditions' => 'Retweets.user_id = retweeter.id'])
            ->order(['Retweets.created' => 'DESC'])
            ->where(['follows.follower_user_id = :user_id'])
            ->bind(':user_id', $user_logged_in->id);




        $posts = $this->Paginator->paginate($query->union($retweet_query)->epilog('ORDER BY created DESC'));
        $liked_posts = $this->Likes
            ->find()
            ->select(['post_id', 'user_id', 'is_retweet'])
            ->where(['user_id' => $user_logged_in->id])
            ->toList();

        $this->set(compact('posts', 'user_logged_in', 'liked_posts'));
    }

    public function view($id, $is_retweet = false)
    {
        $this->loadComponent('Paginator');
        $this->loadModel('Retweets');
        $this->loadModel('Likes');
        $user_logged_in = $this->request->getAttribute('identity');

        $is_retweet = ($is_retweet === '1') ? true : false;


        if ($is_retweet === true) {
            $post = $this->Retweets->findById($id)
                ->contain([
                    'Users',
                    'Likes',
                    'Comments' => [
                        'sort' => ['Comments.created' => 'DESC']
                    ],
                    'Comments.Users',
                    'Posts',
                    'Posts.Users'
                ])
                ->firstOrFail();
        } else {
            $post = $this->Posts->findById($id)
                ->contain([
                    'Users',
                    'Likes',
                    'Comments' => [
                        'sort' => ['Comments.created' => 'DESC']
                    ],
                    'Comments.Users',
                    // 'Posts',
                    // 'Posts.Users',
                    'Retweets'
                ])
                ->firstOrFail();
        }

        $liked_posts = $this->Likes
            ->find()
            ->select(['post_id', 'user_id', 'is_retweet'])
            ->where(['user_id' => $user_logged_in->id])
            ->toList();

        $this->Authorization->skipAuthorization();
        $this->set(compact('post', 'liked_posts', 'user_logged_in'));
    }

    public function add()
    {
        $user = $this->request->getAttribute('identity');
        $post = $this->Posts->newEmptyEntity();
        $this->autoRender = false;
        $this->Authorization->authorize($post);
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->user_id = $user->id;
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Your Post has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to add your post.'));
            $this->redirect($this->referer());
        }
        $this->set('post', $post);
    }
    public function edit($id, $is_retweet = false)
    {
        $is_retweet = ($is_retweet === '1') ? true : false;

        $this->loadModel('Retweets');



        if ($is_retweet === true) {
            $post = $this->Retweets->findById($id)
                ->contain([
                    'Users',
                    'Likes',
                    'Comments' => [
                        'sort' => ['Comments.created' => 'DESC']
                    ],
                    'Comments.Users',
                    'Posts',
                    'Posts.Users'
                ])
                ->firstOrFail();
        } else {
            $post = $this->Posts->findById($id)
                ->contain([
                    'Users',
                    'Likes',
                    'Comments' => [
                        'sort' => ['Comments.created' => 'DESC']
                    ],
                    'Comments.Users',
                    'Retweets'
                ])
                ->firstOrFail();
        }

        $this->Authorization->authorize($post->user);


        if ($this->request->is(['post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($is_retweet === true) {
                $post->retweet_message = $this->request->getData('content');
                $saved_post = $this->Retweets->save($post);
            } else {
                $post->content = $this->request->getData('content');
                $saved_post = $this->Posts->save($post);
            }
            if ($saved_post) {
                $this->Flash->success(__('Your post has been updated.'));
                if ($is_retweet === true) {
                    return $this->redirect(['action' => 'view/' . $id . '/1']);
                }
                return $this->redirect(['action' => 'view/' . $id]);
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        $this->set('post', $post);
    }
    public function retweet($id)
    {
        $this->loadModel('Retweets');
        $posts = $this->Posts->findById($id)->contain(['Users'])->firstOrFail();

        $user_logged_in = $this->request->getAttribute('identity');

        $retweet = $this->Retweets->newEmptyEntity();
        $this->Authorization->authorize($posts);

        $retweet->user_id = $user_logged_in->id;
        $retweet->post_id = $this->request->getData('post_id');
        $retweet->retweet_message = $this->request->getData('retweet_message');

        if ($this->request->is(['post'])) {

            $this->Retweets->patchEntity($posts, $this->request->getData());
            if ($this->Retweets->save($retweet)) {
                $this->Flash->success(__('Post retweeted.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Unable to retweet post.'));
        }
        $this->set('post', $posts);
    }

    public function delete($id, $is_retweet = false)
    {
        $this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Retweets');

        $is_retweet = ($is_retweet === '1') ? true : false;

        if ($is_retweet === true) {
            $post = $this->Retweets->findById($id)
                ->contain(['Users'])
                ->firstOrFail();
            $delete_post = $this->Retweets->delete($post);
        } else {
            $post = $this->Posts->findById($id)
                ->contain(['Users'])
                ->firstOrFail();
            $delete_post = $this->Posts->delete($post);
        }


        $this->Authorization->authorize($post->user);
        if ($delete_post) {
            $this->Flash->success(__('Post has been deleted.'));
            return $this->redirect($this->referer());
        }
    }
    public function like($id, $is_retweet = false)
    {
        $this->Authorization->skipAuthorization();

        $this->autoRender = false;
        $this->loadModel('Likes');
        $user_logged_in = $this->request->getAttribute('identity');

        $post = $this->Likes->newEmptyEntity();
        $post = $this->Likes->patchEntity($post, $this->request->getData());

        $post->user_id = $user_logged_in->id;
        $post->post_id = (int)$id;
        $post->is_retweet = ($is_retweet === '1') ? true : false;

        if ($this->Likes->save($post)) {
            $this->Flash->success(__('Liked succesfully!'));
            return $this->redirect($this->referer());
        }

        $this->Flash->error(__('Error occured while Liking. Please, try again.'));
        $this->redirect($this->referer());
    }
    public function unlike($id, $is_retweet = false)
    {

        $this->Authorization->skipAuthorization();
        $this->autoRender = false;
        $this->loadModel('Likes');
        $user_logged_in = $this->request->getAttribute('identity');

        $id = (int)$id;

        $like = $this->Likes->deleteAll(
            [
                'user_id' => $user_logged_in->id,
                'post_id' => $id,
                'is_retweet' => ($is_retweet === '1') ? true : false
            ]
        );


        if ($like) {
            $this->Flash->success(__('Unliked Successfully.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Error occured while unfollowing. Please, try again.'));
        $this->redirect($this->referer());
    }

    public function isLiked($user_logged_in, $id)
    {
        $this->loadModel('Likes');
        $exist =  $this->Likes->exists([
            'user_id' => $user_logged_in,
            'post_id' => $id
        ]);


        if ($exist) {
            return $id;
        }
    }
    public function isFollowed($user_logged_in, $id)
    {
        $this->loadModel('Follows');
        $exist =  $this->Follows->exists([
            'follower_user_id' => $user_logged_in,
            'followed_user_id' => $id
        ]);
        if ($exist) {
            return $id;
        }
    }
    public function comment()
    {
        $this->loadModel('Comments');
        $this->autoRender = false;
        $post_id = $this->request->getData('id');
        $comment = $this->request->getData('comment');





        $user_logged_in = $this->request->getAttribute('identity');

        $post = $this->Posts->newEmptyEntity();
        $this->Authorization->authorize($post);
        // if ($this->request->is('post')) {
        $post = $this->Comments->patchEntity($post, $this->request->getData());
        $post->id = '';
        $post->user_id = $user_logged_in->id;
        $post->post_id = $post_id;
        $post->comment = $comment;

        if ($this->Comments->save($post)) {
            $this->Flash->success(__('Your Comment has been sent.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Unable to add your post.'));
        // }
        // $this->set('post', $post);
        return $this->redirect($this->referer());
    }
}
