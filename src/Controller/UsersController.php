<?php

declare(strict_types=1);

namespace App\Controller;

// use Cake\Mailer\Email;
use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Utility\Security;
use Cake\Mailer\Mailer;





/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function profile($id = null)
    {
        $this->viewBuilder()->setLayout('main');
        $this->loadModel('Follows');
        $this->loadModel('Likes');
        $this->set('title', 'profile');

        $user_logged_in = $this->request->getAttribute('identity');

        // Check if the current user is followed by the loggedin user
        if ($id === null || (int)$id === $user_logged_in->id) {
            $this->set('title', 'user_profile');
            $id = $user_logged_in->id;
        }

        $is_followed = $this->Follows->exists([
            'follower_user_id' => $user_logged_in->id,
            'followed_user_id' => $id
        ]);

        $user = $this->Users->get($id, [
            'contain' =>
            [
                'Posts' =>
                [
                    'sort' => ['Posts.created' => 'DESC']
                ],
                'Posts.Likes',
                'Posts.Comments',
                'Posts.Retweets',
                'Posts.Users',
                'Retweets' =>
                [
                    'sort' => ['Retweets.created' => 'DESC']
                ],
                'Retweets.Likes',
                'Retweets.Comments',
                'Retweets.Users',
                'Retweets.Posts',
                'Retweets.Posts.Users',
            ],
        ]);

        $all_user_post = array_merge($user->retweets, $user->posts);
        usort($all_user_post, function ($a, $b) {
            $dateA = strtotime($a['created']->format('Y-m-d H:i:s'));
            $dateB = strtotime($b['created']->format('Y-m-d H:i:s'));
            return ($dateB - $dateA);
        });


        $liked_posts = $this->Likes
            ->find()
            ->select(['post_id', 'user_id', 'is_retweet'])
            ->where(['user_id' => $user_logged_in->id])
            ->toList();

        $this->Authorization->authorize($user);

        $followers = $this->Users
            ->find('all')
            ->matching('Follows', function ($q) use ($id) {
                return $q->where(['Follows.followed_user_id' => $id]);
            })
            ->all();

        $following = $this->Users
            ->find('all')
            ->matching('Follows', function ($q) use ($id) {
                return $q->where(['Follows.follower_user_id' => $id]);
            })
            ->all();

        $this->set(compact('followers', 'following', 'user', 'is_followed', 'liked_posts', 'all_user_post', 'user_logged_in'));
    }

    public function add()
    {

        $user = $this->Users->newEmptyEntity();

        $this->Authorization->skipAuthorization();

        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $token = Security::hash(Security::randomBytes(32));
            $user->token = $token;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Registered Successfully!.'));

                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('main');
        $this->set('title', 'edit profile');

        $user = $this->Users->get($id);


        $this->Authorization->authorize($user);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $new_password = $this->request->getData('new_password');
            $old_password = $this->request->getData('old_password');
            $retype_new_password = $this->request->getData('retype_new_password');
            $password_check = (new DefaultPasswordHasher)->check($old_password, $user->password);
            if (!empty($old_password) || !empty($new_password) || !empty($retype_new_password)) {
                if (!$password_check) {
                    $this->Flash->error(__('Change Password Failed! Incorrect password.'));
                    return $this->redirect($this->referer());
                }
                if (empty($old_password) || empty($new_password)) {
                    $this->Flash->error(__('Change Password Failed! Input Required Password Fields.'));
                    return $this->redirect($this->referer());
                }
                if ($new_password !== $retype_new_password) {
                    $this->Flash->error(__('Change Password Failed! Password does not match. Please retype correctly.'));
                    return $this->redirect($this->referer());
                }
                $user->password = $new_password;
            }




            $user = $this->Users->patchEntity($user, $this->request->getData());



            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'profile']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }

        $this->set(compact('user'));
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        // $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }
    public function login()
    {
        $this->Authorization->skipAuthorization();
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        /*
        * Verification - Work in Progress
        *
        */

        // if ($user->token === null) {
        //     return $this->redirect(['action' => 'verify']);
        // }

        $email = $this->request->getData('email');

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid() && $email) {
            // $is_verified = $this->Users->find()
            //     ->select(['email_verified', 'id'])
            //     ->where(['email =' => $email])->first();
            // if ($is_verified->email_verified === null) {
            //     $redirect = $this->request->getQuery('redirect', [
            //         'controller' => 'Users',
            //         'action' => 'verify',
            //         '?' => ['id' => $is_verified->id]
            //     ]);
            // } else {
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Posts',
                'action' => 'index',
            ]);
            // }



            // $user = $result->getData();
            // if ($user->is_verified !== 1) {
            //     $this->Flash->error('User is not active.');
            //     return $this->redirect(['action' => 'verify']);
            // }

            // redirect to /articles after login success


            return $this->redirect($redirect);
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }
    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        $user = $result->getData();

        //this makes assumptions about your user record. 
        //Only you know exactly how this conditional should be expressed.
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    // public function verify()
    // {
    //     // --------------------------- UNDER CONSTRUCTION --------------------------------
    //     // $users = $this->Authentication->getResult();
    //     // $this->set('username', $users);
    //     // debug($users);
    //     // return;

    //     $service = $this->request->getAttribute('authentication');
    //     $this->Authorization->skipAuthorization();
    //     // Will be null on authentication failure, or an authenticator.
    //     // $this->set(compact('users'));

    //     $id = $this->request->getQuery('id');
    //     $user_id = $this->request->getData('id');





    //     if ($this->request->is('post')) {
    //         $user = $this->Users->find()
    //             ->select(['email', 'token'])
    //             ->where(['id =' => $user_id])->first();
    //         $token = $user->token;
    //         $email = $user->email;
    //         // $token = Security::hash(Security::randomBytes(32));

    //         $mailer = new Mailer('default');
    //         $mailer->setFrom(['me@example.com' => 'My Site'])
    //             ->setTo('jerrydieugenio@gmail.com')
    //             ->setSubject('About')
    //             ->deliver('My message ' . $token);
    //         if ($mailer) {
    //             $this->Flash->success(__('Email verification sent...'));
    //         } else {
    //             $this->Flash->error(__('Fail to send email'));
    //         }

    //         $this->set('token', $token);
    //     }


    //     // $user2 = $this->Users->newEmptyEntity();
    //     // $user2 = $this->Users->patchEntity($user2, $this->request->getData());
    //     // $verify = $this->Users->find('all')->where(['token' => $token])->first();
    //     // $verify->verified = '1';
    //     // $this->Users->save($verify);

    // }


    public function followers($id = null)
    {
        //Assignments and Declarations
        $this->Authorization->skipAuthorization();
        $this->loadModel('Follows');
        $this->set('title', 'profile');
        $this->viewBuilder()->setLayout('main');
        $user_logged_in = $this->request->getAttribute('identity');

        // Check if ID is Null in URL or ID in URL is the logged in user
        if ($id == null || $id == $user_logged_in->id) {
            $this->set('title', 'user_profile');
        }

        // Check if the current user is followed by the loggedin user
        $is_followed = $this->Follows->exists([
            'follower_user_id' => $user_logged_in->id,
            'followed_user_id' => $id
        ]);


        // Get User Profile Info 
        $user_profile = $this->Users->get($id);

        // Get All Following
        $following = $this->Users->find('all')
            ->join([
                'a' => [
                    'table' => 'follows',
                    'type' => 'INNER',
                    'conditions' => 'a.followed_user_id  = Users.id AND a.follower_user_id =' . $id
                ]
            ])->all();
        // Get All Followers
        $followers = $this->Users->find('all')
            ->join([
                'a' => [
                    'table' => 'follows',
                    'type' => 'INNER',
                    'conditions' => 'a.follower_user_id  = Users.id AND a.followed_user_id =' . $id
                ]
            ])->all();

        $followed_user_list = [];
        foreach ($followers as $follower) {
            $check = $this->isFollowed($user_logged_in->id, $follower->id);
            if ($check === null) {
                continue;
            }
            array_push($followed_user_list, $this->isFollowed($user_logged_in->id, $follower->id));
        }

        $this->set(compact('following', 'followers', 'user_profile', 'is_followed', 'user_logged_in', 'followed_user_list'));
    }
    public function following($id = null)
    {
        $this->Authorization->skipAuthorization();
        $this->loadModel('Follows');
        $this->set('title', 'profile');
        $this->viewBuilder()->setLayout('main');
        $user_logged_in = $this->request->getAttribute('identity');

        // Check if the user is the logged user
        if ($id == null || $id == $user_logged_in->id) {
            $this->set('title', 'user_profile');
        }

        // Check if the current user is followed by the loggedin user
        $is_followed = $this->Follows->exists([
            'follower_user_id' => $user_logged_in->id,
            'followed_user_id' => $id
        ]);

        // Get User Profile Info 
        $user_profile = $this->Users->get($id);

        // Get All Following
        $following = $this->Users->find('all')->join([
            'a' => [
                'table' => 'follows',
                'type' => 'INNER',
                'conditions' => 'a.followed_user_id  = Users.id AND a.follower_user_id =' . $id
            ]
        ])->all();

        // Get All Followers
        $followers = $this->Users->find('all')
            ->matching('Follows', function ($q) use ($id) {
                return $q->where(['Follows.followed_user_id' => $id]);
            })
            ->all();

        $this->set(compact('following', 'followers', 'user_profile', 'is_followed', 'user_logged_in'));
    }

    public function isFollowed($user_logged_in, $id)
    {
        $exist =  $this->Follows->exists([
            'follower_user_id' => $user_logged_in,
            'followed_user_id' => $id
        ]);
        if ($exist) {
            return $id;
        }
    }


    public function follow($id)
    {
        $this->Authorization->skipAuthorization();

        $this->autoRender = false;
        $this->loadModel('Follows');
        $user_logged_in = $this->request->getAttribute('identity');

        $check = $this->Follows->exists([
            'follower_user_id' => $user_logged_in->id,
            'followed_user_id' => $id
        ]);
        if ($check) {
            return;
        }
        $user = $this->Follows->newEmptyEntity();
        $user = $this->Follows->patchEntity($user, $this->request->getData());

        $user->follower_user_id = $user_logged_in->id;
        $user->followed_user_id = (int)$id;

        if ($this->Follows->save($user)) {
            $this->Flash->success(__('Followed succesfully!'));
            return $this->redirect($this->referer());
        }

        $this->Flash->error(__('Error occured while following. Please, try again.'));
        $this->redirect($this->referer());
    }

    public function unfollow($id)
    {

        $this->Authorization->skipAuthorization();
        $this->autoRender = false;
        $this->loadModel('Follows');
        $user_logged_in = $this->request->getAttribute('identity');

        $id = (int)$id;

        $follow = $this->Follows->deleteAll(
            [
                'follower_user_id = ' => $user_logged_in->id,
                'followed_user_id = ' => $id
            ]
        );

        if ($follow) {
            $this->Flash->success(__('Unfollowed Successfully.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('Error occured while unfollowing. Please, try again.'));
        $this->redirect($this->referer());
    }
}
