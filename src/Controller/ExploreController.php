<?php

namespace App\Controller;

use App\Controller\AppController;

class ExploreController extends AppController
{
    public $paginate = [
        'limit' => 3,
        'order' => [
            'Users.username' => 'asc'
        ]
    ];
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->set('title', 'posts');
        $this->viewBuilder()->setLayout('main');
        $this->Authorization->skipAuthorization();
    }
    public function index()
    {
        $this->loadModel('Users');
        $this->loadModel('Follows');
        $key = $this->request->getQuery('key');
        $user_logged_in = $this->request->getAttribute('identity');

        if ($key) {
            $query = $this->Users
                ->find('all')
                ->distinct()
                ->join(['table' => 'follows', 'type' => 'LEFT', 'conditions' => 'follows.follower_user_id != :user_id AND Users.id != :user_id'])
                ->where(['username like :key'])
                ->bind(':user_id', $user_logged_in->id)
                ->bind(':key', '%' . $key . '%');
        } else {
            $query = $this->Users
                ->find('all')
                ->distinct()
                ->contain(['Follows'])
                ->join(['table' => 'follows', 'type' => 'LEFT', 'conditions' => 'follows.follower_user_id != :user_id AND Users.id != :user_id'])
                ->bind(':user_id', $user_logged_in->id);
        }

        $users = $this->paginate($query);
        $followed_users = $this->Follows
            ->find()
            ->select(['followed_user_id'])
            ->where(['follower_user_id' => $user_logged_in->id])
            ->toList();

        $this->set(compact('users', 'user_logged_in', 'followed_users'));
    }
    public function search()
    {
        $this->loadModel('Users');
        $key = $this->request->getQuery('key');
        if ($key) {
            $query = $this->Users
                ->find('all')
                ->where(['username like' => '%' . $key . '%']);
        } else {
            $query = $this->Users;
        }

        $users = $this->paginate($query);
        $this->set(compact('users'));
    }
    public function users()
    {
        $this->index();
    }
    public function posts()
    {
    }
}
